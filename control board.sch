EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Raspberry_Pi_2_3 J1
U 1 1 5F66D9F6
P 1850 2250
F 0 "J1" H 1850 3850 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 1850 3750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 1850 2250 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 1850 2250 50  0001 C CNN
	1    1850 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3550 1550 3550
Wire Wire Line
	1550 3550 1650 3550
Connection ~ 1550 3550
Wire Wire Line
	1650 3550 1750 3550
Connection ~ 1650 3550
Wire Wire Line
	1750 3550 1850 3550
Connection ~ 1750 3550
Wire Wire Line
	1850 3550 1950 3550
Connection ~ 1850 3550
Wire Wire Line
	1950 3550 2050 3550
Connection ~ 1950 3550
Wire Wire Line
	2050 3550 2150 3550
Connection ~ 2050 3550
Text GLabel 2050 900  2    50   Input ~ 0
+3.3V
Wire Wire Line
	1950 950  2050 950 
Wire Wire Line
	2050 900  2050 950 
Connection ~ 2050 950 
Text GLabel 3500 1650 2    50   Input ~ 0
SDA
Text GLabel 3500 1750 2    50   Input ~ 0
SCL
$Comp
L Device:R_Small R2
U 1 1 5F67DB32
P 3350 1500
F 0 "R2" H 3300 1700 50  0000 L CNN
F 1 "2k" V 3450 1450 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 3350 1500 50  0001 C CNN
F 3 "~" H 3350 1500 50  0001 C CNN
	1    3350 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5F67E1E3
P 3200 1500
F 0 "R1" H 3150 1700 50  0000 L CNN
F 1 "2k" V 3100 1450 50  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 3200 1500 50  0001 C CNN
F 3 "~" H 3200 1500 50  0001 C CNN
	1    3200 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1750 3200 1750
Wire Wire Line
	3200 1600 3200 1750
Wire Wire Line
	2650 1650 3350 1650
Wire Wire Line
	3350 1600 3350 1650
Wire Wire Line
	3200 1400 3350 1400
Text GLabel 3500 1400 2    50   Input ~ 0
+3.3V
Wire Wire Line
	3200 1750 3500 1750
Connection ~ 3200 1750
Wire Wire Line
	3350 1650 3500 1650
Connection ~ 3350 1650
Wire Wire Line
	3350 1400 3500 1400
Connection ~ 3350 1400
Text Notes 1150 4050 0    50   ~ 0
STEMMA i2c ports
$Comp
L power:GND #PWR0101
U 1 1 5F6967E9
P 1450 3550
F 0 "#PWR0101" H 1450 3300 50  0001 C CNN
F 1 "GND" H 1455 3377 50  0000 C CNN
F 2 "" H 1450 3550 50  0001 C CNN
F 3 "" H 1450 3550 50  0001 C CNN
	1    1450 3550
	1    0    0    -1  
$EndComp
Connection ~ 1450 3550
Text GLabel 1650 900  0    50   Input ~ 0
+5V
Wire Wire Line
	1650 950  1750 950 
Wire Wire Line
	1650 900  1650 950 
Connection ~ 1650 950 
$Comp
L power:GND #PWR0102
U 1 1 5F69D172
P 2100 4500
F 0 "#PWR0102" H 2100 4250 50  0001 C CNN
F 1 "GND" H 2105 4327 50  0000 C CNN
F 2 "" H 2100 4500 50  0001 C CNN
F 3 "" H 2100 4500 50  0001 C CNN
	1    2100 4500
	1    0    0    -1  
$EndComp
Text GLabel 2100 4400 2    50   Input ~ 0
+3.3V
Text GLabel 2100 4200 2    50   Input ~ 0
SCL
Text GLabel 2100 4300 2    50   Input ~ 0
SDA
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5F69D165
P 1900 4300
F 0 "J2" H 1900 4500 50  0000 C CNN
F 1 "I2C" H 1900 3950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.00mm" H 1900 4300 50  0001 C CNN
F 3 "~" H 1900 4300 50  0001 C CNN
	1    1900 4300
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
